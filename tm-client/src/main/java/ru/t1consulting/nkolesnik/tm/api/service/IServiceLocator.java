package ru.t1consulting.nkolesnik.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1consulting.nkolesnik.tm.api.endpoint.*;

public interface IServiceLocator {

    @NotNull
    ICommandService getCommandService();

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    ITaskEndpoint getTaskEndpoint();

    @NotNull
    IProjectEndpoint getProjectEndpoint();

    @NotNull
    IProjectTaskEndpoint getProjectTaskEndpoint();

    @NotNull
    IUserEndpoint getUserEndpoint();

    @NotNull
    IAuthEndpoint getAuthEndpoint();

    @NotNull
    ISystemEndpoint getSystemEndpoint();

    @NotNull
    IDomainEndpoint getDomainEndpoint();

    @NotNull
    ITokenService getTokenService();

}
