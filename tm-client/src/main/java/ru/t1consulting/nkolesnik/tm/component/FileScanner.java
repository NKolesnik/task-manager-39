package ru.t1consulting.nkolesnik.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1consulting.nkolesnik.tm.command.AbstractCommand;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class FileScanner {

    @NotNull
    private final Bootstrap bootstrap;

    @NotNull
    private final File folder = new File("./");

    @NotNull
    private final List<String> commands = new ArrayList<>();

    @NotNull
    private final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

    public FileScanner(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    private void init() {
        @NotNull final Iterable<AbstractCommand> commands = bootstrap.getCommandService().getCommandsWithArguments();
        commands.forEach(e -> this.commands.add(e.getName()));
        executorService.scheduleWithFixedDelay(this::process, 0, 3, TimeUnit.SECONDS);
    }

    @SneakyThrows
    private void process() {
        for (File file : folder.listFiles()) {
            if (file.isDirectory()) continue;
            @NotNull final String fileName = file.getName();
            final boolean checkName = commands.contains(fileName);
            if (checkName) {
                file.delete();
                bootstrap.processCommand(fileName);
            }
        }
    }

    public void start() {
        init();
    }

}
