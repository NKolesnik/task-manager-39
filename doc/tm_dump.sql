--
-- PostgreSQL database dump
--

-- Dumped from database version 13.3
-- Dumped by pg_dump version 13.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: projects; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.projects (
    id text NOT NULL,
    user_id text NOT NULL,
    name text DEFAULT ''::text NOT NULL,
    description text DEFAULT ''::text NOT NULL,
    status text DEFAULT 'Not started'::text NOT NULL,
    date_begin time without time zone,
    date_end timestamp without time zone
);


ALTER TABLE public.projects OWNER TO postgres;

--
-- Name: TABLE projects; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE public.projects IS 'Task Manager Projects';


--
-- Name: tasks; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tasks (
    id text NOT NULL,
    user_id text,
    name text DEFAULT ''::text NOT NULL,
    description text DEFAULT ''::text NOT NULL,
    project_id text,
    status text DEFAULT 'Not started'::text NOT NULL,
    created timestamp without time zone DEFAULT LOCALTIMESTAMP NOT NULL,
    date_begin timestamp without time zone,
    date_end timestamp without time zone
);


ALTER TABLE public.tasks OWNER TO postgres;

--
-- Name: TABLE tasks; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE public.tasks IS 'Task Manager Tasks';


--
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.users (
    id text NOT NULL,
    login text,
    password_hash text,
    email text,
    fst_name text,
    mid_name text,
    last_name text,
    role text NOT NULL,
    locked_flg boolean NOT NULL
);


ALTER TABLE public.users OWNER TO postgres;

--
-- Name: TABLE users; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE public.users IS 'Task Manager users';


--
-- Data for Name: projects; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.projects (id, user_id, name, description, status, date_begin, date_end) FROM stdin;
da3757a9-1935-4ddd-b545-61a93a09e701	ac22837d-0c80-4407-93d1-29fd13e17ada	admin project	admin project description	NOT_STARTED	\N	\N
\.


--
-- Data for Name: tasks; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tasks (id, user_id, name, description, project_id, status, created, date_begin, date_end) FROM stdin;
da3757a9-1935-4ddd-b545-61a93a09e702	ac22837d-0c80-4407-93d1-29fd13e17ada	ADMIN TASK	ADMIN TASK DESCRIPTION	da3757a9-1935-4ddd-b545-61a93a09e701	IN_PROGRESS	2021-11-02 21:49:33.516701	2021-11-02 21:49:33.516701	2021-11-02 21:49:33.516701
da3757a9-1935-4ddd-b545-61a93a09e712	ac22837d-0c80-4407-93d1-29fd13e17ada	ADMIN TASK	ADMIN TASK DESCRIPTION	da3757a9-1935-4ddd-b545-61a93a09e701	IN_PROGRESS	2021-11-02 21:49:37.476429	2021-11-02 21:49:37.476429	2021-11-02 21:49:37.476429
da3757a9-1935-4ddd-b545-61a93a09e752	ac22837d-0c80-4407-93d1-29fd13e17ada	ADMIN TASK	ADMIN TASK DESCRIPTION	da3757a9-1935-4ddd-b545-61a93a09e701	IN_PROGRESS	2021-11-02 21:49:40.559579	2021-11-02 21:49:40.559579	2021-11-02 21:49:40.559579
da3757a9-1935-4ddd-b545-61a93a09e722	ac22837d-0c80-4407-93d1-29fd13e17ada	ADMIN TASK	ADMIN TASK DESCRIPTION	da3757a9-1935-4ddd-b545-61a93a09e701	IN_PROGRESS	2021-11-02 21:49:42.511524	2021-11-02 21:49:42.511524	2021-11-02 21:49:42.511524
da3757a9-1935-4ddd-b545-61a93a09e721	ac22837d-0c80-4407-93d1-29fd13e17ada	ADMIN TASK	ADMIN TASK DESCRIPTION	da3757a9-1935-4ddd-b545-61a93a09e701	IN_PROGRESS	2021-11-02 21:49:44.257995	2021-11-02 21:49:44.257995	2021-11-02 21:49:44.257995
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.users (id, login, password_hash, email, fst_name, mid_name, last_name, role, locked_flg) FROM stdin;
2505b21e-8496-45bf-a89b-43ce1bb9c464	test	52dd2e870d49b025b5050999acf378c3	test@test.email.ru	\N	\N	\N	USUAL	f
ac22837d-0c80-4407-93d1-29fd13e17ada	admin	1b15716a3edccd7cf2a9a496161ad13a	admin@admin.email.ru	\N	\N	\N	ADMIN	f
\.


--
-- Name: projects project_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.projects
    ADD CONSTRAINT project_id_key UNIQUE (id);


--
-- Name: projects project_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.projects
    ADD CONSTRAINT project_pkey PRIMARY KEY (id);


--
-- Name: tasks task_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tasks
    ADD CONSTRAINT task_id_key UNIQUE (id);


--
-- Name: tasks task_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tasks
    ADD CONSTRAINT task_pkey PRIMARY KEY (id);


--
-- Name: users unique_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT unique_id UNIQUE (id);


--
-- Name: users user_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT user_pkey PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--

