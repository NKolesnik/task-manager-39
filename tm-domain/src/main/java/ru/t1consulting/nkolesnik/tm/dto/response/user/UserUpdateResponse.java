package ru.t1consulting.nkolesnik.tm.dto.response.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.dto.response.AbstractUserResponse;
import ru.t1consulting.nkolesnik.tm.model.User;

@NoArgsConstructor
public final class UserUpdateResponse extends AbstractUserResponse {

    public UserUpdateResponse(@Nullable final User user) {
        super(user);
    }

}

