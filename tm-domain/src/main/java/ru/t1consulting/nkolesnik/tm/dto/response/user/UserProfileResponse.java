package ru.t1consulting.nkolesnik.tm.dto.response.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.dto.response.AbstractUserResponse;
import ru.t1consulting.nkolesnik.tm.model.User;

@NoArgsConstructor
public class UserProfileResponse extends AbstractUserResponse {

    public UserProfileResponse(@Nullable final User user) {
        super(user);
    }

}
