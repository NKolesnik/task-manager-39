package ru.t1consulting.nkolesnik.tm.exception.field;

public final class RoleIsEmptyException extends AbstractFieldException {

    public RoleIsEmptyException() {
        super("Error! Role is empty...");
    }

}
