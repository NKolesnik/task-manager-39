package ru.t1consulting.nkolesnik.tm.dto.response.data;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.dto.request.AbstractUserRequest;

@NoArgsConstructor
public class DataDatabaseSaveBackupResponse extends AbstractUserRequest {

    public DataDatabaseSaveBackupResponse(@Nullable String token) {
        super(token);
    }

}
