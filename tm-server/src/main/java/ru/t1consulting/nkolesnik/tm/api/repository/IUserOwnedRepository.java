package ru.t1consulting.nkolesnik.tm.api.repository;

import ru.t1consulting.nkolesnik.tm.model.AbstractUserOwnedModel;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

}
