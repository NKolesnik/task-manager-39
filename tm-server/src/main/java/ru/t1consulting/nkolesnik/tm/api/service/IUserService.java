package ru.t1consulting.nkolesnik.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.enumerated.Role;
import ru.t1consulting.nkolesnik.tm.model.User;

import java.util.Collection;
import java.util.List;

public interface IUserService extends IService<User> {

    void add(@Nullable User user);

    void addALl(@Nullable Collection<User> users);

    void set(@Nullable Collection<User> users);

    @NotNull
    User create(@Nullable String userId, @Nullable String name);

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable Role role);

    @NotNull
    User create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable String email, @Nullable Role role);

    long getSize();

    @Nullable
    List<User> findAll();

    @Nullable
    User findById(@Nullable String id);

    @Nullable
    User findByLogin(@Nullable String login);

    @Nullable
    User findByEmail(@Nullable String email);

    @NotNull
    Boolean isLoginExist(@Nullable String login);

    @NotNull
    Boolean isEmailExist(@Nullable String email);

    void setPassword(@Nullable String id, @Nullable String password);

    @NotNull
    User updateUser(
            @Nullable String id,
            @Nullable String firstName,
            @Nullable String middleName,
            @Nullable String lastName
    );

    void update(@Nullable User user);

    void lockUserByLogin(@Nullable String login);

    void unlockUserByLogin(String login);

    void clear();

    void remove(@Nullable User user);

    void removeByLogin(@Nullable String login);

    void removeById(@Nullable String id);

}
