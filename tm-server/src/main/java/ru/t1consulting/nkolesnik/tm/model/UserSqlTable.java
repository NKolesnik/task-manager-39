package ru.t1consulting.nkolesnik.tm.model;

import org.mybatis.dynamic.sql.SqlColumn;
import org.mybatis.dynamic.sql.SqlTable;

import java.sql.JDBCType;

public class UserSqlTable extends SqlTable {

    public final SqlColumn<String> id = column("id", JDBCType.VARCHAR);
    public final SqlColumn<String> login = column("login", JDBCType.VARCHAR);
    public final SqlColumn<String> passwordHash = column("password_hash", JDBCType.VARCHAR);
    public final SqlColumn<String> email = column("email", JDBCType.VARCHAR);
    public final SqlColumn<String> firstName = column("fst_name", JDBCType.VARCHAR);
    public final SqlColumn<String> middleName = column("mid_name", JDBCType.VARCHAR);
    public final SqlColumn<String> lastName = column("last_name", JDBCType.VARCHAR);
    public final SqlColumn<String> role = column("role", JDBCType.VARCHAR);
    public final SqlColumn<Boolean> locked = column("locked_flg", JDBCType.BOOLEAN);

    public UserSqlTable() {
        super("users");
    }

}
