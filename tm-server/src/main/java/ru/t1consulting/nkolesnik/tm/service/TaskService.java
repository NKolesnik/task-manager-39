package ru.t1consulting.nkolesnik.tm.service;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.mybatis.dynamic.sql.SortSpecification;
import org.mybatis.dynamic.sql.SqlBuilder;
import org.mybatis.dynamic.sql.render.RenderingStrategy;
import org.mybatis.dynamic.sql.select.render.SelectStatementProvider;
import org.mybatis.dynamic.sql.update.render.UpdateStatementProvider;
import ru.t1consulting.nkolesnik.tm.api.repository.ITaskRepository;
import ru.t1consulting.nkolesnik.tm.api.service.IConnectionService;
import ru.t1consulting.nkolesnik.tm.api.service.ITaskService;
import ru.t1consulting.nkolesnik.tm.comparator.CreatedComparator;
import ru.t1consulting.nkolesnik.tm.comparator.DateBeginComparator;
import ru.t1consulting.nkolesnik.tm.comparator.StatusComparator;
import ru.t1consulting.nkolesnik.tm.enumerated.Sort;
import ru.t1consulting.nkolesnik.tm.enumerated.Status;
import ru.t1consulting.nkolesnik.tm.exception.entity.StatusNotFoundException;
import ru.t1consulting.nkolesnik.tm.exception.entity.TaskNotFoundException;
import ru.t1consulting.nkolesnik.tm.exception.entity.UserNotFoundException;
import ru.t1consulting.nkolesnik.tm.exception.field.*;
import ru.t1consulting.nkolesnik.tm.model.ProjectProvider;
import ru.t1consulting.nkolesnik.tm.model.Task;
import ru.t1consulting.nkolesnik.tm.model.TaskProvider;

import java.util.*;

import static org.mybatis.dynamic.sql.SqlBuilder.isEqualTo;
import static org.mybatis.dynamic.sql.SqlBuilder.select;
import static ru.t1consulting.nkolesnik.tm.model.ProjectProvider.*;
import static ru.t1consulting.nkolesnik.tm.model.TaskProvider.projectId;
import static ru.t1consulting.nkolesnik.tm.model.TaskProvider.tasks;

public final class TaskService extends AbstractUserOwnedService<Task, ITaskRepository> implements ITaskService {

    public TaskService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @Override
    public void add(@Nullable final Task task) {
        if (task == null) throw new TaskNotFoundException();
        @NotNull final SqlSession session = getSqlSession();
        try {
            @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
            repository.add(task);
            session.commit();
        } catch (Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public void add(@Nullable final String userId, @Nullable final Task task) {
        if (task == null) throw new TaskNotFoundException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final SqlSession session = getSqlSession();
        try {
            @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
            repository.addForUser(userId, task);
            session.commit();
        } catch (Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public void add(@Nullable Collection<Task> tasks) {
        if (tasks == null || tasks.isEmpty()) throw new TaskNotFoundException();
        @NotNull final SqlSession session = getSqlSession();
        try {
            @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
            for (Task task : tasks)
                repository.add(task);
            session.commit();
        } catch (Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public void add(@Nullable final String userId, @Nullable Collection<Task> tasks) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (tasks == null || tasks.isEmpty()) throw new TaskNotFoundException();
        @NotNull final SqlSession session = getSqlSession();
        try {
            @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
            for (Task task : tasks)
                repository.addForUser(userId, task);
            session.commit();
        } catch (Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public void set(@Nullable Collection<Task> tasks) {
        if (tasks == null || tasks.isEmpty()) throw new TaskNotFoundException();
        @NotNull final SqlSession session = getSqlSession();
        try {
            @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
            repository.clear();
            for (Task task : tasks)
                repository.add(task);
            session.commit();
        } catch (Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public void set(@Nullable final String userId, @Nullable Collection<Task> tasks) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (tasks == null || tasks.isEmpty()) throw new TaskNotFoundException();
        @NotNull final SqlSession session = getSqlSession();
        try {
            @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
            repository.clear();
            for (Task task : tasks)
                repository.addForUser(userId, task);
            session.commit();
        } catch (Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task create(
            @Nullable final String userId,
            @Nullable final String name
    ) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final SqlSession session = getSqlSession();
        @NotNull final Task task = new Task();
        try {
            @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
            task.setName(name);
            task.setUserId(userId);
            repository.add(task);
            session.commit();
        } catch (Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final SqlSession session = getSqlSession();
        @NotNull final Task task = new Task();
        try {
            @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
            task.setName(name);
            task.setUserId(userId);
            task.setDescription(description);
            repository.add(task);
            session.commit();
        } catch (Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description,
            @Nullable final Date dateBegin,
            @Nullable final Date dateEnd
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final SqlSession session = getSqlSession();
        @NotNull final Task task = new Task();
        try {
            @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
            task.setName(name);
            task.setUserId(userId);
            task.setDescription(description);
            task.setDateBegin(dateBegin);
            task.setDateEnd(dateEnd);
            repository.add(task);
            session.commit();
        } catch (Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
        return task;
    }

    @Override
    public long getSize() {
        try (@NotNull final SqlSession session = getSqlSession()) {
            @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
            return repository.getSize();
        }
    }

    @Override
    public long getSize(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try (@NotNull final SqlSession session = getSqlSession()) {
            @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
            return repository.getSizeForUser(userId);
        }
    }

    @NotNull
    @Override
    public List<Task> findAll() {
        @Nullable final List<Task> tasks;
        try (@NotNull final SqlSession session = getSqlSession()) {
            @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
            tasks = repository.findAll();
            if (tasks == null || tasks.isEmpty()) return Collections.emptyList();
            return tasks;
        }
    }

    @NotNull
    @Override
    public List<Task> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @Nullable final List<Task> tasks;
        try (@NotNull final SqlSession session = getSqlSession()) {
            @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
            tasks = repository.findAllForUser(userId);
            if (tasks == null || tasks.isEmpty()) return Collections.emptyList();
            return tasks;
        }
    }

    @NotNull
    @Override
    public List<Task> findAll(@Nullable Comparator<Task> comparator) {
        if (comparator == null) return findAll();
        @Nullable final List<Task> taskList;
        try (@NotNull final SqlSession session = getSqlSession()) {
            @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
            @NotNull SelectStatementProvider statement =
                    select(id, TaskProvider.userId, name, description, status, created, dateBegin, dateEnd).
                            from(tasks).
                            orderBy(getSortSpec(comparator)).
                            build().
                            render(RenderingStrategy.MYBATIS3);
            taskList = repository.findAllWithCriteria(statement);
            if (taskList == null || taskList.isEmpty()) return Collections.emptyList();
            return taskList;
        }
    }

    @NotNull
    @Override
    public List<Task> findAll(@Nullable Sort sort) {
        if (sort == null) return findAll();
        @Nullable final List<Task> taskList;
        try (@NotNull final SqlSession session = getSqlSession()) {
            @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
            @NotNull SelectStatementProvider statement =
                    select(id, TaskProvider.userId, name, description, status, created, dateBegin, dateEnd).
                            from(tasks).
                            orderBy(getSortSpec(sort.getComparator())).
                            build().
                            render(RenderingStrategy.MYBATIS3);
            taskList = repository.findAllWithCriteria(statement);
            if (taskList == null || taskList.isEmpty()) return Collections.emptyList();
            return taskList;
        }
    }

    @NotNull
    @Override
    public List<Task> findAll(@Nullable final String userId, @Nullable Comparator<Task> comparator) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (comparator == null) return findAll();
        @Nullable final List<Task> taskList;
        try (@NotNull final SqlSession session = getSqlSession()) {
            @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
            @NotNull SelectStatementProvider statement =
                    select(id, TaskProvider.userId, name, description, status, created, dateBegin, dateEnd).
                            from(tasks).
                            where(TaskProvider.userId, isEqualTo(userId)).
                            orderBy(getSortSpec(comparator)).
                            build().
                            render(RenderingStrategy.MYBATIS3);
            taskList = repository.findAllWithCriteria(statement);
            if (taskList == null || taskList.isEmpty()) return Collections.emptyList();
            return taskList;
        }
    }

    @NotNull
    @Override
    public List<Task> findAll(@Nullable final String userId, @Nullable Sort sort) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (sort == null) return findAll(userId);
        @Nullable final List<Task> taskList;
        try (@NotNull final SqlSession session = getSqlSession()) {
            @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
            @NotNull SelectStatementProvider statement =
                    select(id, TaskProvider.userId, name, description, status, created, dateBegin, dateEnd).
                            from(tasks).
                            where(TaskProvider.userId, isEqualTo(userId)).
                            orderBy(getSortSpec(sort.getComparator())).
                            build().
                            render(RenderingStrategy.MYBATIS3);
            taskList = repository.findAllWithCriteria(statement);
            if (taskList == null || taskList.isEmpty()) return Collections.emptyList();
            return taskList;
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAllByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        Optional.ofNullable(userId).orElseThrow(UserNotFoundException::new);
        if (projectId == null || projectId.isEmpty()) return findAll(userId);
        @Nullable List<Task> result;
        try (@NotNull final SqlSession session = getSqlSession()) {
            @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
            result = repository.findAllByProjectId(userId, projectId);
            if (result == null) return Collections.emptyList();
            return result;
        }
    }

    @Nullable
    @Override
    public Task findById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new TaskIdEmptyException();
        try (@NotNull final SqlSession session = getSqlSession()) {
            @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
            return repository.findById(id);
        }
    }

    @Nullable
    @Override
    public Task findById(@Nullable final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new TaskIdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try (@NotNull final SqlSession session = getSqlSession()) {
            @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
            return repository.findByIdForUser(userId, id);
        }
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return false;
        try (@NotNull final SqlSession session = getSqlSession()) {
            @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
            return repository.existsById(id);
        }
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) return false;
        if (userId == null || userId.isEmpty()) return false;
        try (@NotNull final SqlSession session = getSqlSession()) {
            @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
            return repository.existsByIdForUser(userId, id);
        }
    }

    @Override
    public void update(@Nullable final Task task) {
        if (task == null) throw new TaskNotFoundException();
        @NotNull final SqlSession session = getSqlSession();
        try {
            @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
            @NotNull final UpdateStatementProvider statement =
                    SqlBuilder.update(tasks).
                            set(name).equalTo(task.getName()).
                            set(description).equalTo(task.getDescription()).
                            set(status).equalTo(task.getStatus()).
                            set(userId).equalTo(task.getUserId()).
                            set(created).equalTo(task.getCreated()).
                            set(projectId).equalTo(task.getProjectId()).
                            set(dateBegin).equalTo(task.getDateBegin()).
                            set(dateEnd).equalTo(task.getDateEnd()).
                            where(id, isEqualTo(task.getId())).
                            build().render(RenderingStrategy.MYBATIS3);
            repository.update(statement);
            session.commit();
        } catch (Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    @SneakyThrows
    public void updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (id == null || id.isEmpty()) throw new TaskIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final SqlSession session = getSqlSession();
        try {
            @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
            @Nullable final Task task = repository.findByIdForUser(userId, id);
            if (task == null) throw new TaskNotFoundException();
            @NotNull final UpdateStatementProvider statement =
                    SqlBuilder.update(tasks).
                            set(ProjectProvider.name).equalTo(name).
                            set(ProjectProvider.description).equalTo(description).
                            where(ProjectProvider.id, isEqualTo(task.getId())).
                            build().render(RenderingStrategy.MYBATIS3);
            repository.update(statement);
            session.commit();
        } catch (Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    @SneakyThrows
    public void changeTaskStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (status == null) throw new StatusNotFoundException();
        if (id == null || id.isEmpty()) throw new TaskIdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final SqlSession session = getSqlSession();
        try {
            @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
            @Nullable final Task task = repository.findByIdForUser(userId, id);
            if (task == null) throw new TaskNotFoundException();
            @NotNull final UpdateStatementProvider statement =
                    SqlBuilder.update(tasks).
                            set(ProjectProvider.status).equalTo(status).
                            where(ProjectProvider.id, isEqualTo(task.getId())).
                            build().render(RenderingStrategy.MYBATIS3);
            repository.update(statement);
            session.commit();
        } catch (Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public void clear() {
        @NotNull final SqlSession session = getSqlSession();
        try {
            @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
            repository.clear();
            session.commit();
        } catch (Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final SqlSession session = getSqlSession();
        try {
            @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
            repository.clearForUser(userId);
            session.commit();
        } catch (Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public void remove(@Nullable final Task task) {
        if (task == null) throw new TaskNotFoundException();
        @NotNull final SqlSession session = getSqlSession();
        try {
            @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
            @NotNull final String id = task.getId();
            repository.removeById(id);
            session.commit();
        } catch (Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final Task task) {
        if (task == null) throw new TaskNotFoundException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final SqlSession session = getSqlSession();
        try {
            @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
            @NotNull final String id = task.getId();
            repository.removeByIdForUser(userId, id);
            session.commit();
        } catch (Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public void removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new TaskIdEmptyException();
        @NotNull final SqlSession session = getSqlSession();
        try {
            @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
            @Nullable final Task repositoryTask = findById(id);
            if (repositoryTask == null) throw new TaskNotFoundException();
            repository.removeById(id);
            session.commit();
        } catch (Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public void removeById(@Nullable final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new TaskIdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final SqlSession session = getSqlSession();
        try {
            @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
            @Nullable final Task repositoryTask = repository.findByIdForUser(userId, id);
            if (repositoryTask == null) throw new TaskNotFoundException();
            repository.removeByIdForUser(userId, id);
            session.commit();
        } catch (Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public void removeByProjectId(@Nullable String userId, @Nullable String projectId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        @NotNull final SqlSession session = getSqlSession();
        try {
            @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
            repository.removeByProjectId(userId, projectId);
            session.commit();
        } catch (Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }


    @NotNull
    protected SortSpecification getSortSpec(@NotNull Comparator comparator) {
        if (comparator == CreatedComparator.INSTANCE) return created;
        else if (comparator == StatusComparator.INSTANCE) return status;
        else if (comparator == DateBeginComparator.INSTANCE) return dateBegin;
        else return name;
    }


}
