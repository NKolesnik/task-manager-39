package ru.t1consulting.nkolesnik.tm.service;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.mybatis.dynamic.sql.SortSpecification;
import org.mybatis.dynamic.sql.SqlBuilder;
import org.mybatis.dynamic.sql.render.RenderingStrategy;
import org.mybatis.dynamic.sql.select.render.SelectStatementProvider;
import org.mybatis.dynamic.sql.update.render.UpdateStatementProvider;
import ru.t1consulting.nkolesnik.tm.api.repository.IProjectRepository;
import ru.t1consulting.nkolesnik.tm.api.service.IConnectionService;
import ru.t1consulting.nkolesnik.tm.api.service.IProjectService;
import ru.t1consulting.nkolesnik.tm.comparator.CreatedComparator;
import ru.t1consulting.nkolesnik.tm.comparator.DateBeginComparator;
import ru.t1consulting.nkolesnik.tm.comparator.StatusComparator;
import ru.t1consulting.nkolesnik.tm.enumerated.Sort;
import ru.t1consulting.nkolesnik.tm.enumerated.Status;
import ru.t1consulting.nkolesnik.tm.exception.entity.ProjectNotFoundException;
import ru.t1consulting.nkolesnik.tm.exception.entity.StatusNotFoundException;
import ru.t1consulting.nkolesnik.tm.exception.field.DescriptionEmptyException;
import ru.t1consulting.nkolesnik.tm.exception.field.NameEmptyException;
import ru.t1consulting.nkolesnik.tm.exception.field.ProjectIdEmptyException;
import ru.t1consulting.nkolesnik.tm.exception.field.UserIdEmptyException;
import ru.t1consulting.nkolesnik.tm.model.Project;
import ru.t1consulting.nkolesnik.tm.model.ProjectProvider;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static org.mybatis.dynamic.sql.SqlBuilder.isEqualTo;
import static org.mybatis.dynamic.sql.SqlBuilder.select;
import static ru.t1consulting.nkolesnik.tm.model.ProjectProvider.*;

public final class ProjectService extends AbstractUserOwnedService<Project, IProjectRepository> implements IProjectService {

    public ProjectService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @Override
    public void add(@Nullable final Project project) {
        if (project == null) throw new ProjectNotFoundException();
        @NotNull final SqlSession session = getSqlSession();
        try {
            @NotNull final IProjectRepository repository = session.getMapper(IProjectRepository.class);
            repository.add(project);
            session.commit();
        } catch (Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public void add(@Nullable final String userId, @Nullable final Project project) {
        if (project == null) throw new ProjectNotFoundException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final SqlSession session = getSqlSession();
        try {
            @NotNull final IProjectRepository repository = session.getMapper(IProjectRepository.class);
            repository.addForUser(userId, project);
            session.commit();
        } catch (Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public void add(@Nullable Collection<Project> projects) {
        if (projects == null || projects.isEmpty()) throw new ProjectNotFoundException();
        @NotNull final SqlSession session = getSqlSession();
        try {
            @NotNull final IProjectRepository repository = session.getMapper(IProjectRepository.class);
            for (Project project : projects)
                repository.add(project);
            session.commit();
        } catch (Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public void add(@Nullable final String userId, @Nullable Collection<Project> projects) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projects == null || projects.isEmpty()) throw new ProjectNotFoundException();
        @NotNull final SqlSession session = getSqlSession();
        try {
            @NotNull final IProjectRepository repository = session.getMapper(IProjectRepository.class);
            for (Project project : projects)
                repository.addForUser(userId, project);
            session.commit();
        } catch (Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public void set(@Nullable Collection<Project> projects) {
        if (projects == null || projects.isEmpty()) throw new ProjectNotFoundException();
        @NotNull final SqlSession session = getSqlSession();
        try {
            @NotNull final IProjectRepository repository = session.getMapper(IProjectRepository.class);
            repository.clear();
            for (Project project : projects)
                repository.add(project);
            session.commit();
        } catch (Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public void set(@Nullable final String userId, @Nullable Collection<Project> projects) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projects == null || projects.isEmpty()) throw new ProjectNotFoundException();
        @NotNull final SqlSession session = getSqlSession();
        try {
            @NotNull final IProjectRepository repository = session.getMapper(IProjectRepository.class);
            repository.clear();
            for (Project project : projects)
                repository.addForUser(userId, project);
            session.commit();
        } catch (Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project create(
            @Nullable final String userId,
            @Nullable final String name
    ) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final SqlSession session = getSqlSession();
        @NotNull final Project project = new Project();
        try {
            @NotNull final IProjectRepository repository = session.getMapper(IProjectRepository.class);
            project.setName(name);
            project.setUserId(userId);
            repository.add(project);
            session.commit();
        } catch (Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final SqlSession session = getSqlSession();
        @NotNull final Project project = new Project();
        try {
            @NotNull final IProjectRepository repository = session.getMapper(IProjectRepository.class);
            project.setName(name);
            project.setUserId(userId);
            project.setDescription(description);
            repository.add(project);
            session.commit();
        } catch (Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
        return project;
    }

    @Override
    public long getSize() {
        try (@NotNull final SqlSession session = getSqlSession()) {
            @NotNull final IProjectRepository repository = session.getMapper(IProjectRepository.class);
            return repository.getSize();
        }
    }

    @Override
    public long getSize(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try (@NotNull final SqlSession session = getSqlSession()) {
            @NotNull final IProjectRepository repository = session.getMapper(IProjectRepository.class);
            return repository.getSizeForUser(userId);
        }
    }

    @NotNull
    @Override
    public List<Project> findAll() {
        @Nullable final List<Project> projects;
        try (@NotNull final SqlSession session = getSqlSession()) {
            @NotNull final IProjectRepository repository = session.getMapper(IProjectRepository.class);
            projects = repository.findAll();
            if (projects == null || projects.isEmpty()) return Collections.emptyList();
            return projects;
        }
    }

    @NotNull
    @Override
    public List<Project> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @Nullable final List<Project> projects;
        try (@NotNull final SqlSession session = getSqlSession()) {
            @NotNull final IProjectRepository repository = session.getMapper(IProjectRepository.class);
            projects = repository.findAllForUser(userId);
            if (projects == null || projects.isEmpty()) return Collections.emptyList();
            return projects;
        }
    }

    @NotNull
    @Override
    public List<Project> findAll(@Nullable Comparator<Project> comparator) {
        if (comparator == null) return findAll();
        @Nullable final List<Project> projectList;
        try (@NotNull final SqlSession session = getSqlSession()) {
            @NotNull final IProjectRepository repository = session.getMapper(IProjectRepository.class);
            @NotNull SelectStatementProvider statement =
                    select(id, ProjectProvider.userId, name, description, status, created, dateBegin, dateEnd).
                            from(projects).
                            orderBy(getSortSpec(comparator)).
                            build().
                            render(RenderingStrategy.MYBATIS3);
            projectList = repository.findAllWithCriteria(statement);
            if (projectList == null || projectList.isEmpty()) return Collections.emptyList();
            return projectList;
        }
    }

    @NotNull
    @Override
    public List<Project> findAll(@Nullable Sort sort) {
        if (sort == null) return findAll();
        @Nullable final List<Project> projectList;
        try (@NotNull final SqlSession session = getSqlSession()) {
            @NotNull final IProjectRepository repository = session.getMapper(IProjectRepository.class);
            @NotNull SelectStatementProvider statement =
                    select(id, ProjectProvider.userId, name, description, status, created, dateBegin, dateEnd).
                            from(projects).
                            orderBy(getSortSpec(sort.getComparator())).
                            build().
                            render(RenderingStrategy.MYBATIS3);
            projectList = repository.findAllWithCriteria(statement);
            if (projectList == null || projectList.isEmpty()) return Collections.emptyList();
            return projectList;
        }
    }

    @NotNull
    @Override
    public List<Project> findAll(@Nullable final String userId, @Nullable Comparator<Project> comparator) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (comparator == null) return findAll();
        @Nullable final List<Project> projectList;
        try (@NotNull final SqlSession session = getSqlSession()) {
            @NotNull final IProjectRepository repository = session.getMapper(IProjectRepository.class);
            @NotNull SelectStatementProvider statement =
                    select(id, ProjectProvider.userId, name, description, status, created, dateBegin, dateEnd).
                            from(projects).
                            where(ProjectProvider.userId, isEqualTo(userId)).
                            orderBy(getSortSpec(comparator)).
                            build().
                            render(RenderingStrategy.MYBATIS3);
            projectList = repository.findAllWithCriteria(statement);
            if (projectList == null || projectList.isEmpty()) return Collections.emptyList();
            return projectList;
        }
    }

    @NotNull
    @Override
    public List<Project> findAll(@Nullable final String userId, @Nullable Sort sort) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (sort == null) return findAll(userId);
        @Nullable final List<Project> projectList;
        try (@NotNull final SqlSession session = getSqlSession()) {
            @NotNull final IProjectRepository repository = session.getMapper(IProjectRepository.class);
            @NotNull SelectStatementProvider statement =
                    select(id, ProjectProvider.userId, name, description, status, created, dateBegin, dateEnd).
                            from(projects).
                            where(ProjectProvider.userId, isEqualTo(userId)).
                            orderBy(getSortSpec(sort.getComparator())).
                            build().
                            render(RenderingStrategy.MYBATIS3);
            projectList = repository.findAllWithCriteria(statement);
            if (projectList == null || projectList.isEmpty()) return Collections.emptyList();
            return projectList;
        }
    }

    @Nullable
    @Override
    public Project findById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new ProjectIdEmptyException();
        try (@NotNull final SqlSession session = getSqlSession()) {
            @NotNull final IProjectRepository repository = session.getMapper(IProjectRepository.class);
            return repository.findById(id);
        }
    }

    @Nullable
    @Override
    public Project findById(@Nullable final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new ProjectIdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try (@NotNull final SqlSession session = getSqlSession()) {
            @NotNull final IProjectRepository repository = session.getMapper(IProjectRepository.class);
            return repository.findByIdForUser(userId, id);
        }
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return false;
        try (@NotNull final SqlSession session = getSqlSession()) {
            @NotNull final IProjectRepository repository = session.getMapper(IProjectRepository.class);
            return repository.existsById(id);
        }
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) return false;
        if (userId == null || userId.isEmpty()) return false;
        try (@NotNull final SqlSession session = getSqlSession()) {
            @NotNull final IProjectRepository repository = session.getMapper(IProjectRepository.class);
            return repository.existsByIdForUser(userId, id);
        }
    }

    @Override
    public void update(@Nullable final Project project) {
        if (project == null) throw new ProjectNotFoundException();
        @NotNull final SqlSession session = getSqlSession();
        try {
            @NotNull final IProjectRepository repository = session.getMapper(IProjectRepository.class);
            @NotNull final UpdateStatementProvider statement =
                    SqlBuilder.update(projects).
                            set(name).equalTo(project.getName()).
                            set(description).equalTo(project.getDescription()).
                            set(status).equalTo(project.getStatus()).
                            set(userId).equalTo(project.getUserId()).
                            set(created).equalTo(project.getCreated()).
                            set(dateBegin).equalTo(project.getDateBegin()).
                            set(dateEnd).equalTo(project.getDateEnd()).
                            where(id, isEqualTo(project.getId())).
                            build().render(RenderingStrategy.MYBATIS3);
            repository.update(statement);
            session.commit();
        } catch (Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    @SneakyThrows
    public void updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (id == null || id.isEmpty()) throw new ProjectIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final SqlSession session = getSqlSession();
        try {
            @NotNull final IProjectRepository repository = session.getMapper(IProjectRepository.class);
            @Nullable final Project project = repository.findByIdForUser(userId, id);
            if (project == null) throw new ProjectNotFoundException();
            @NotNull final UpdateStatementProvider statement =
                    SqlBuilder.update(projects).
                            set(ProjectProvider.name).equalTo(name).
                            set(ProjectProvider.description).equalTo(description).
                            where(ProjectProvider.id, isEqualTo(project.getId())).
                            build().render(RenderingStrategy.MYBATIS3);
            repository.update(statement);
            session.commit();
        } catch (Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    @SneakyThrows
    public void changeProjectStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (status == null) throw new StatusNotFoundException();
        if (id == null || id.isEmpty()) throw new ProjectIdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final SqlSession session = getSqlSession();
        try {
            @NotNull final IProjectRepository repository = session.getMapper(IProjectRepository.class);
            @Nullable final Project project = repository.findByIdForUser(userId, id);
            if (project == null) throw new ProjectNotFoundException();
            @NotNull final UpdateStatementProvider statement =
                    SqlBuilder.update(projects).
                            set(ProjectProvider.status).equalTo(status).
                            where(ProjectProvider.id, isEqualTo(project.getId())).
                            build().render(RenderingStrategy.MYBATIS3);
            repository.update(statement);
            session.commit();
        } catch (Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public void clear() {
        @NotNull final SqlSession session = getSqlSession();
        try {
            @NotNull final IProjectRepository repository = session.getMapper(IProjectRepository.class);
            repository.clear();
            session.commit();
        } catch (Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final SqlSession session = getSqlSession();
        try {
            @NotNull final IProjectRepository repository = session.getMapper(IProjectRepository.class);
            repository.clearForUser(userId);
            session.commit();
        } catch (Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public void remove(@Nullable final Project project) {
        if (project == null) throw new ProjectNotFoundException();
        @NotNull final SqlSession session = getSqlSession();
        try {
            @NotNull final IProjectRepository repository = session.getMapper(IProjectRepository.class);
            repository.remove(project);
            session.commit();
        } catch (Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final Project project) {
        if (project == null) throw new ProjectNotFoundException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final SqlSession session = getSqlSession();
        try {
            @NotNull final IProjectRepository repository = session.getMapper(IProjectRepository.class);
            repository.removeForUser(userId, project);
            session.commit();
        } catch (Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public void removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new ProjectIdEmptyException();
        @NotNull final SqlSession session = getSqlSession();
        try {
            @NotNull final IProjectRepository repository = session.getMapper(IProjectRepository.class);
            @Nullable final Project project = repository.findById(id);
            if (project == null) throw new ProjectNotFoundException();
            repository.removeById(id);
            session.commit();
        } catch (Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public void removeById(@Nullable final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new ProjectIdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final SqlSession session = getSqlSession();
        try {
            @NotNull final IProjectRepository repository = session.getMapper(IProjectRepository.class);
            @Nullable final Project project = repository.findByIdForUser(userId, id);
            if (project == null) throw new ProjectNotFoundException();
            repository.removeByIdForUser(userId, id);
            session.commit();
        } catch (Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }


    @NotNull
    protected SortSpecification getSortSpec(@NotNull Comparator comparator) {
        if (comparator == CreatedComparator.INSTANCE) return created;
        else if (comparator == StatusComparator.INSTANCE) return status;
        else if (comparator == DateBeginComparator.INSTANCE) return dateBegin;
        else return name;
    }

}
