package ru.t1consulting.nkolesnik.tm.model;

import org.mybatis.dynamic.sql.SqlColumn;
import ru.t1consulting.nkolesnik.tm.enumerated.Status;

import java.util.Date;

public class TaskProvider {

    public static final TaskSqlTable tasks = new TaskSqlTable();

    public static final SqlColumn<String> id = tasks.id;

    public static final SqlColumn<String> userId = tasks.userId;

    public static final SqlColumn<String> name = tasks.name;

    public static final SqlColumn<String> description = tasks.description;

    public static final SqlColumn<String> projectId = tasks.projectId;

    public static final SqlColumn<Status> status = tasks.status;

    public static final SqlColumn<Date> created = tasks.created;

    public static final SqlColumn<Date> dateBegin = tasks.dateBegin;

    public static final SqlColumn<Date> dateEnd = tasks.dateEnd;

}
