package ru.t1consulting.nkolesnik.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.enumerated.Sort;
import ru.t1consulting.nkolesnik.tm.enumerated.Status;
import ru.t1consulting.nkolesnik.tm.model.Project;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IProjectService extends IUserOwnedService<Project> {

    void add(@Nullable Project project);

    void add(@Nullable String userId, @Nullable Project project);

    void add(@Nullable Collection<Project> projects);

    void add(@Nullable String userId, @Nullable Collection<Project> projects);

    void set(@Nullable Collection<Project> projects);

    void set(@Nullable String userId, @Nullable Collection<Project> projects);

    @NotNull
    Project create(@Nullable String userId, @Nullable String name);

    @NotNull
    Project create(@Nullable String userId, @Nullable String name, @Nullable String description);

    long getSize();

    long getSize(@Nullable String userId);

    @NotNull
    List<Project> findAll();

    @NotNull
    List<Project> findAll(@Nullable String userId);

    @NotNull
    List<Project> findAll(@Nullable Comparator<Project> comparator);

    @NotNull
    List<Project> findAll(@Nullable Sort sort);

    @NotNull
    List<Project> findAll(@Nullable String userId, @Nullable Comparator<Project> comparator);

    @NotNull
    List<Project> findAll(@Nullable String userId, @Nullable Sort sort);

    @Nullable
    Project findById(@Nullable String id);

    @Nullable
    Project findById(@Nullable String userId, @Nullable String id);

    boolean existsById(@Nullable String id);

    boolean existsById(@Nullable String userId, @Nullable String id);

    void update(@Nullable Project project);

    void updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

    void changeProjectStatusById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable Status status
    );

    void clear();

    void clear(@Nullable String userId);

    void remove(@Nullable Project project);

    void remove(@Nullable String userId, @Nullable Project project);

    void removeById(@Nullable String id);

    void removeById(@Nullable String userId, @Nullable String id);


}
