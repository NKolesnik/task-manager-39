package ru.t1consulting.nkolesnik.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.mybatis.dynamic.sql.select.render.SelectStatementProvider;
import org.mybatis.dynamic.sql.update.render.UpdateStatementProvider;
import org.mybatis.dynamic.sql.util.SqlProviderAdapter;
import ru.t1consulting.nkolesnik.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<Task> {

    @Insert("INSERT INTO tasks (id, created, name, description, project_id, status, user_id, date_begin, date_end)"
            + "VALUES (#{id}, #{created}, #{name}, #{description}, #{projectId}, #{status}, #{userId}, #{dateBegin}, #{dateEnd});")
    void add(@Nullable Task task);

    @Insert("INSERT INTO tasks (id, created, name, description, project_id, status, user_id, date_begin, date_end)"
            + "VALUES (#{task.id}, #{task.created}, #{task.name}," +
            " #{task.description}, #{task.projectId}, #{task.status}, #{userId}," +
            " #{task.dateBegin}, #{task.dateEnd});")
    void addForUser(@Param("userId") @Nullable String userId, @Param("task") @Nullable Task task);

    @Select("SELECT COUNT(1) FROM tasks;")
    long getSize();

    @Select("SELECT COUNT(1) FROM tasks WHERE user_id = #{userId};")
    long getSizeForUser(@Param("userId") @Nullable String userId);

    @Nullable
    @Select("SELECT id, user_id, name, description, project_id, status, date_begin, date_end, created FROM tasks;")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "dateBegin", column = "date_begin"),
            @Result(property = "dateEnd", column = "date_end")
    })
    List<Task> findAll();

    @Nullable
    @Select("SELECT id, user_id, name, description, project_id, status, date_begin, date_end, created " +
            "FROM tasks where user_id = #{userId};")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "dateBegin", column = "date_begin"),
            @Result(property = "dateEnd", column = "date_end")
    })
    List<Task> findAllForUser(@Param("userId") @Nullable String userId);

    @Nullable
    @SelectProvider(type = SqlProviderAdapter.class, method = "select")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "dateBegin", column = "date_begin"),
            @Result(property = "dateEnd", column = "date_end")
    })
    List<Task> findAllWithCriteria(@Nullable SelectStatementProvider statement);

    @Nullable
    @Select("SELECT id, user_id, name, description, project_id, status, date_begin, date_end, created " +
            "FROM tasks WHERE id = #{id};")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "dateBegin", column = "date_begin"),
            @Result(property = "dateEnd", column = "date_end")
    })
    Task findById(@Param("id") @Nullable String id);

    @Nullable
    @Select("SELECT id, user_id, name, description, project_id, status, date_begin, date_end, created " +
            "FROM tasks WHERE id = #{id} AND user_id = #{userId};")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "dateBegin", column = "date_begin"),
            @Result(property = "dateEnd", column = "date_end")
    })
    Task findByIdForUser(@Param("userId") @Nullable String userId, @Param("id") @Nullable String id);

    @Select("SELECT COUNT(1)=1 FROM tasks WHERE Id = #{id};")
    boolean existsById(@Param("id") @Nullable String id);

    @Select("SELECT COUNT(1)=1 FROM tasks WHERE Id = #{id} AND user_id = #{userId};")
    boolean existsByIdForUser(@Param("userId") @Nullable String userId, @Param("id") @Nullable String id);

    @UpdateProvider(type = SqlProviderAdapter.class, method = "update")
    void update(@NotNull UpdateStatementProvider statement);

    @Delete("DELETE FROM tasks;")
    void clear();

    @Delete("DELETE FROM tasks WHERE user_id = #{userId};")
    void clearForUser(@Param("userId") @Nullable String userId);

    @Delete("DELETE FROM tasks WHERE id = #{task.id};")
    void remove(@Param("task") @Nullable Task task);

    @Delete("DELETE FROM tasks WHERE id = #{task.id} AND user_id = #{userId};")
    void removeForUser(@Param("userId") @Nullable String userId, @Param("task") @Nullable Task task);

    @Delete("DELETE FROM tasks WHERE id = #{id};")
    void removeById(@Param("id") @Nullable String id);

    @Delete("DELETE FROM tasks WHERE id = #{id} AND user_id = #{userId};")
    void removeByIdForUser(@Param("userId") @Nullable String userId, @Param("id") @Nullable String id);

    @Nullable
    @Select("SELECT id, user_id, name, description, project_id, status, date_begin, date_end, created" +
            " FROM tasks WHERE project_id = #{projectId} AND user_id = #{userId};")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "dateBegin", column = "date_begin"),
            @Result(property = "dateEnd", column = "date_end")
    })
    List<Task> findAllByProjectId(
            @Param("userId") @Nullable String userId,
            @Param("projectId") @Nullable String projectId
    );

    @Delete("DELETE FROM tasks WHERE project_id = #{projectId} AND user_id = #{userId};")
    void removeByProjectId(@Param("userId") @Nullable String userId, @Param("projectId") @Nullable String projectId);

}
