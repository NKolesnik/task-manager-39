package ru.t1consulting.nkolesnik.tm.model;

import org.mybatis.dynamic.sql.SqlColumn;

public class UserProvider {

    public static final UserSqlTable users = new UserSqlTable();

    public static final SqlColumn<String> id = users.id;

    public static final SqlColumn<String> login = users.login;

    public static final SqlColumn<String> passwordHash = users.passwordHash;

    public static final SqlColumn<String> email = users.email;

    public static final SqlColumn<String> firstName = users.firstName;

    public static final SqlColumn<String> middleName = users.middleName;

    public static final SqlColumn<String> lastName = users.lastName;

    public static final SqlColumn<String> role = users.role;

    public static final SqlColumn<Boolean> locked = users.locked;


}
