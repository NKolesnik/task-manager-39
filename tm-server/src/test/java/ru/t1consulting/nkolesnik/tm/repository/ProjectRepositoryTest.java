package ru.t1consulting.nkolesnik.tm.repository;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.mybatis.dynamic.sql.SortSpecification;
import org.mybatis.dynamic.sql.render.RenderingStrategy;
import org.mybatis.dynamic.sql.select.render.SelectStatementProvider;
import org.mybatis.dynamic.sql.update.render.UpdateStatementProvider;
import ru.t1consulting.nkolesnik.tm.api.repository.IProjectRepository;
import ru.t1consulting.nkolesnik.tm.api.service.IConnectionService;
import ru.t1consulting.nkolesnik.tm.api.service.IDomainService;
import ru.t1consulting.nkolesnik.tm.api.service.IPropertyService;
import ru.t1consulting.nkolesnik.tm.comparator.CreatedComparator;
import ru.t1consulting.nkolesnik.tm.comparator.DateBeginComparator;
import ru.t1consulting.nkolesnik.tm.comparator.StatusComparator;
import ru.t1consulting.nkolesnik.tm.enumerated.Status;
import ru.t1consulting.nkolesnik.tm.marker.DataCategory;
import ru.t1consulting.nkolesnik.tm.model.Project;
import ru.t1consulting.nkolesnik.tm.model.ProjectProvider;
import ru.t1consulting.nkolesnik.tm.model.User;
import ru.t1consulting.nkolesnik.tm.service.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;

import static org.mybatis.dynamic.sql.SqlBuilder.*;
import static ru.t1consulting.nkolesnik.tm.model.ProjectProvider.*;

@Category(DataCategory.class)
public class ProjectRepositoryTest {

    @NotNull
    private static final String PROJECT_NAME_PREFIX = "TEST_PROJECT_NAME";

    @NotNull
    private static final String PROJECT_DESCRIPTION_PREFIX = "TEST_PROJECT_DESCRIPTION";

    @Nullable
    private static final String NULL_PROJECT_ID = null;

    @Nullable
    private static final String NULL_USER_ID = null;

    @Nullable
    private static final String BAD_USER_ID = UUID.randomUUID().toString();

    @Nullable
    private static final String BAD_PROJECT_ID = UUID.randomUUID().toString();

    @Nullable
    private static final Project NULL_PROJECT = null;

    private static final long REPOSITORY_SIZE = 1000L;

    private static final long EMPTY_REPOSITORY_SIZE = 0L;

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private static String TEST_USER_ID = "";

    @NotNull
    private static String projectId = "";

    @NotNull
    private List<Project> projectList;

    @NotNull
    private Project project;

    @BeforeClass
    public static void prepareTestEnvironment() {
        getTestData();
        saveDbBackup();
    }

    @AfterClass
    public static void restoreProdEnvironment() {
        loadDbBackup();
    }

    @SneakyThrows
    public static void saveDbBackup() {
        @NotNull final String userName = propertyService.getDatabaseUsername();
        @NotNull final String userPassword = propertyService.getDatabasePassword();
        @NotNull final String url = propertyService.getDatabaseConnectionString();
        @NotNull final Connection connection = DriverManager.getConnection(url, userName, userPassword);
        connection.setAutoCommit(false);
        String sqlSaveProjectData = "SELECT * INTO \"backup_projects\" FROM \"projects\";";
        String sqlClearProjectTable = "DELETE FROM \"projects\";";
        String sqlSaveTaskData = "SELECT * INTO \"backup_tasks\" FROM \"tasks\";";
        String sqlClearTaskTable = "DELETE FROM \"tasks\";";
        try {
            Statement statement = connection.createStatement();
            statement.executeUpdate(sqlSaveTaskData);
            statement.executeUpdate(sqlSaveProjectData);
            statement.executeUpdate(sqlClearTaskTable);
            statement.executeUpdate(sqlClearProjectTable);
            connection.commit();
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    public static void loadDbBackup() {
        @NotNull final String userName = propertyService.getDatabaseUsername();
        @NotNull final String userPassword = propertyService.getDatabasePassword();
        @NotNull final String url = propertyService.getDatabaseConnectionString();
        @NotNull final Connection connection = DriverManager.getConnection(url, userName, userPassword);
        connection.setAutoCommit(false);
        @NotNull final String sqlClearProjectTable = "DELETE FROM \"projects\";";
        @NotNull final String sqlLoadProjectData = "INSERT INTO \"projects\" (SELECT * FROM \"backup_projects\");";
        @NotNull final String sqlDropBackupProjectTable = "DROP TABLE \"backup_projects\";";
        @NotNull final String sqlClearTaskTable = "DELETE FROM \"tasks\";";
        @NotNull final String sqlLoadTaskData = "INSERT INTO \"tasks\" (SELECT * FROM \"backup_tasks\");";
        @NotNull final String sqlDropBackupTaskTable = "DROP TABLE \"backup_tasks\";";
        try {
            Statement statement = connection.createStatement();
            statement.executeUpdate(sqlClearTaskTable);
            statement.executeUpdate(sqlClearProjectTable);
            statement.executeUpdate(sqlLoadProjectData);
            statement.executeUpdate(sqlLoadTaskData);
            statement.executeUpdate(sqlDropBackupTaskTable);
            statement.executeUpdate(sqlDropBackupProjectTable);
            connection.commit();
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    public static void getTestData() {
        @NotNull final String userName = propertyService.getDatabaseUsername();
        @NotNull final String userPassword = propertyService.getDatabasePassword();
        @NotNull final String url = propertyService.getDatabaseConnectionString();
        @NotNull final Connection connection = DriverManager.getConnection(url, userName, userPassword);
        connection.setAutoCommit(false);
        String sqlGetTestUserIdData = "SELECT id FROM \"users\" WHERE \"login\" ='test';";
        try {
            Statement statement = connection.createStatement();
            @NotNull final ResultSet userSet = statement.executeQuery(sqlGetTestUserIdData);
            if(userSet.next()){
                TEST_USER_ID = userSet.getString(1);
            }
            String sqlGetTestProjectIdData = "SELECT id FROM \"projects\" WHERE \"user_id\" = '"+TEST_USER_ID+"';";
            @NotNull final ResultSet projectSet = statement.executeQuery(sqlGetTestProjectIdData);
            if(projectSet.next()){
                projectId = projectSet.getString(1);
            }
            connection.commit();
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Before
    public void setup() {
        project = createOneProject();
        projectList = createManyProjects();
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IProjectRepository repository = session.getMapper(IProjectRepository.class);
            repository.clear();
        }
    }

    @After
    public void cleanup() {
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IProjectRepository repository = session.getMapper(IProjectRepository.class);
            repository.clear();
        }
    }

    @Test
    public void add() {
        @NotNull final SqlSession session = connectionService.getSqlSession();
        try {
            @NotNull final IProjectRepository repository = session.getMapper(IProjectRepository.class);
            Assert.assertThrows(Exception.class, () -> repository.add(NULL_PROJECT));
            session.rollback();
            Assert.assertThrows(Exception.class, () -> repository.addForUser(NULL_USER_ID, NULL_PROJECT));
            session.rollback();
            Assert.assertThrows(Exception.class, () -> repository.addForUser(BAD_USER_ID, createOneProject()));
            session.rollback();
            @NotNull final Project project = new Project();
            repository.clear();
            repository.add(project);
            Assert.assertEquals(1, repository.getSize());
            session.commit();
            @NotNull final User user = new User();
            @NotNull final Project projectWithUser = new Project();
            repository.clear();
            repository.addForUser(TEST_USER_ID, projectWithUser);
            Assert.assertEquals(1, repository.getSize());
            repository.clear();
            session.commit();
        } catch (Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Test
    public void getSize() {
        @NotNull final SqlSession session = connectionService.getSqlSession();
        try {
            @NotNull final IProjectRepository repository = session.getMapper(IProjectRepository.class);
            Assert.assertEquals(EMPTY_REPOSITORY_SIZE, repository.getSize());
            session.commit();
            for (Project project : projectList)
                repository.add(project);
            Assert.assertEquals(REPOSITORY_SIZE, repository.getSize());
            session.commit();
            repository.clear();
            session.commit();
            Assert.assertEquals(EMPTY_REPOSITORY_SIZE, repository.getSizeForUser(TEST_USER_ID));
            session.commit();
            for (Project project : projectList)
                repository.addForUser(TEST_USER_ID, project);
            Assert.assertEquals(REPOSITORY_SIZE, repository.getSizeForUser(TEST_USER_ID));
            repository.clear();
            session.commit();
        } catch (Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Test
    public void findAll() {
        @NotNull final SqlSession session = connectionService.getSqlSession();
        try {
            @NotNull final IProjectRepository repository = session.getMapper(IProjectRepository.class);
            for (Project project : projectList)
                repository.add(project);
            @Nullable List<Project> repositoryProjects = repository.findAll();
            Assert.assertNotNull(repositoryProjects);
            Assert.assertEquals(REPOSITORY_SIZE, repositoryProjects.size());
            session.commit();
            repository.clear();
            session.commit();
            repositoryProjects = repository.findAllForUser(TEST_USER_ID);
            Assert.assertEquals(EMPTY_REPOSITORY_SIZE, repositoryProjects.size());
            session.commit();
            for (Project project : projectList) {
                repository.addForUser(TEST_USER_ID, project);
            }
            repositoryProjects = repository.findAllForUser(TEST_USER_ID);
            Assert.assertEquals(REPOSITORY_SIZE, repositoryProjects.size());
            repository.clear();
            session.commit();
        } catch (Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Test
    public void findAllWithCriteria() {
        @NotNull final SqlSession session = connectionService.getSqlSession();
        try {
            @NotNull final IProjectRepository repository = session.getMapper(IProjectRepository.class);
            for (Project project : projectList)
                repository.add(project);

            @NotNull SelectStatementProvider statement =
                    select(id, ProjectProvider.userId, name, description, status, created, dateBegin, dateEnd).
                            from(projects).
                            orderBy(getSortSpec(CreatedComparator.INSTANCE)).
                            build().
                            render(RenderingStrategy.MYBATIS3);
            @Nullable List<Project> repositoryProjects = repository.findAllWithCriteria(statement);
            Assert.assertNotNull(repositoryProjects);
            Assert.assertEquals(REPOSITORY_SIZE, repositoryProjects.size());
            session.commit();
            repository.clear();
            session.commit();
            statement =
                    select(id, ProjectProvider.userId, name, description, status, created, dateBegin, dateEnd).
                            from(projects).
                            where(ProjectProvider.userId, isEqualTo(TEST_USER_ID)).
                            orderBy(getSortSpec(CreatedComparator.INSTANCE)).
                            build().
                            render(RenderingStrategy.MYBATIS3);
            repositoryProjects = repository.findAllWithCriteria(statement);
            Assert.assertEquals(EMPTY_REPOSITORY_SIZE, repositoryProjects.size());
            session.commit();
            for (Project project : projectList) {
                repository.addForUser(TEST_USER_ID, project);
            }
            repositoryProjects = repository.findAllWithCriteria(statement);
            Assert.assertEquals(REPOSITORY_SIZE, repositoryProjects.size());
            repository.clear();
            session.commit();
        } catch (Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Test
    public void findById() {
        @NotNull final SqlSession session = connectionService.getSqlSession();
        try {
            @NotNull final IProjectRepository repository = session.getMapper(IProjectRepository.class);
            repository.add(project);
            Assert.assertNull(repository.findById(NULL_PROJECT_ID));
            session.commit();
            Assert.assertNull(repository.findById(BAD_PROJECT_ID));
            session.commit();
            Assert.assertNull(repository.findByIdForUser(NULL_USER_ID, projectId));
            session.commit();
            Assert.assertNull(repository.findByIdForUser(BAD_USER_ID, projectId));
            session.commit();
            @Nullable Project repositoryProject = repository.findById(projectId);
            Assert.assertNotNull(repositoryProject);
            Assert.assertEquals(projectId, repositoryProject.getId());
            session.commit();
            repositoryProject = repository.findByIdForUser(TEST_USER_ID, projectId);
            Assert.assertNotNull(repositoryProject);
            Assert.assertEquals(projectId, repositoryProject.getId());
            session.commit();
            for (Project project : projectList) {
                repository.addForUser(TEST_USER_ID, project);
            }
            repositoryProject = repository.findByIdForUser(TEST_USER_ID, projectId);
            Assert.assertNotNull(repositoryProject);
            Assert.assertEquals(projectId, repositoryProject.getId());
            repository.clear();
            session.commit();
        } catch (Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Test
    public void existsById() {
        @NotNull final SqlSession session = connectionService.getSqlSession();
        try {
            @NotNull final IProjectRepository repository = session.getMapper(IProjectRepository.class);
            repository.add(project);
            session.commit();
            Assert.assertFalse(repository.existsById(NULL_PROJECT_ID));
            Assert.assertFalse(repository.existsById(BAD_PROJECT_ID));
            Assert.assertFalse(repository.existsByIdForUser(NULL_USER_ID, projectId));
            Assert.assertFalse(repository.existsByIdForUser(BAD_USER_ID, projectId));
            Assert.assertTrue(repository.existsById(projectId));
            Assert.assertTrue(repository.existsByIdForUser(TEST_USER_ID, projectId));
            repository.clear();
            session.commit();
        } catch (Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Test
    public void updateProject() {
        @NotNull final String newName = "NEW PROJECT NAME";
        @NotNull final String newDescription = "NEW PROJECT DESCRIPTION";
        @NotNull final Status newStatus = Status.COMPLETED;
        @NotNull final Date newDateEnd = new Date();
        @Nullable final String oldName;
        @Nullable final String oldDescription;
        @Nullable final Status oldStatus;
        @Nullable final Date oldDateEnd;
        @NotNull final SqlSession session = connectionService.getSqlSession();
        try {
            @NotNull final IProjectRepository repository = session.getMapper(IProjectRepository.class);
            repository.add(project);
            session.commit();
            oldName = project.getName();
            oldDescription = project.getDescription();
            oldStatus = project.getStatus();
            oldDateEnd = project.getDateEnd();
            @NotNull UpdateStatementProvider statement =
                    update(projects).
                            set(name).equalTo(newName).
                            set(description).equalTo(newDescription).
                            set(status).equalTo(newStatus).
                            set(dateEnd).equalTo(newDateEnd).
                            where(id, isEqualTo(projectId)).
                            build().render(RenderingStrategy.MYBATIS3);
            repository.update(statement);
            session.commit();
            @Nullable final Project repositoryProject = repository.findById(projectId);
            Assert.assertNotNull(repositoryProject);
            Assert.assertEquals(newName, repositoryProject.getName());
            Assert.assertEquals(newDescription, repositoryProject.getDescription());
            Assert.assertEquals(newStatus, repositoryProject.getStatus());
            Assert.assertEquals(newDateEnd, repositoryProject.getDateEnd());
            Assert.assertNotEquals(oldName, repositoryProject.getName());
            Assert.assertNotEquals(oldDescription, repositoryProject.getDescription());
            Assert.assertNotEquals(oldStatus, repositoryProject.getStatus());
            Assert.assertNotEquals(oldDateEnd, repositoryProject.getDateEnd());
            statement =
                    update(projects).
                            set(name).equalTo(oldName).
                            set(description).equalTo(oldDescription).
                            set(status).equalTo(oldStatus).
                            set(dateEnd).equalTo(oldDateEnd).
                            where(id, isEqualTo(projectId)).
                            build().render(RenderingStrategy.MYBATIS3);
            repository.update(statement);
            session.commit();
            repository.clear();
            session.commit();
        } catch (Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Test
    public void clear() {
        @NotNull final SqlSession session = connectionService.getSqlSession();
        try {
            @NotNull final IProjectRepository repository = session.getMapper(IProjectRepository.class);
            for (Project project : projectList) {
                repository.addForUser(TEST_USER_ID, project);
            }
            session.commit();
            Assert.assertEquals(REPOSITORY_SIZE, repository.getSize());
            repository.clear();
            Assert.assertEquals(EMPTY_REPOSITORY_SIZE, repository.getSize());
            session.commit();
            for (Project project : projectList) {
                repository.addForUser(TEST_USER_ID, project);
            }
            session.commit();
            Assert.assertEquals(REPOSITORY_SIZE, repository.getSizeForUser(TEST_USER_ID));
            repository.clear();
            Assert.assertEquals(EMPTY_REPOSITORY_SIZE, repository.getSizeForUser(TEST_USER_ID));
            session.commit();
        } catch (Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Test
    public void remove() {
        @NotNull final SqlSession session = connectionService.getSqlSession();
        try {
            @NotNull final IProjectRepository repository = session.getMapper(IProjectRepository.class);
            repository.add(project);
            session.commit();
            Assert.assertEquals(1, repository.getSize());
            repository.remove(project);
            Assert.assertEquals(EMPTY_REPOSITORY_SIZE, repository.getSize());
            session.commit();
            repository.addForUser(TEST_USER_ID, project);
            session.commit();
            Assert.assertEquals(1, repository.getSize());
            repository.removeForUser(TEST_USER_ID, project);
            Assert.assertEquals(EMPTY_REPOSITORY_SIZE, repository.getSize());
            repository.clear();
            session.commit();
        } catch (Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Test
    public void removeById() {
        @NotNull final SqlSession session = connectionService.getSqlSession();
        try {
            @NotNull final IProjectRepository repository = session.getMapper(IProjectRepository.class);
            repository.add(project);
            session.commit();
            Assert.assertEquals(1, repository.getSize());
            repository.removeById(projectId);
            Assert.assertEquals(EMPTY_REPOSITORY_SIZE, repository.getSize());
            session.commit();
            repository.addForUser(TEST_USER_ID, project);
            session.commit();
            Assert.assertEquals(1, repository.getSize());
            repository.removeByIdForUser(TEST_USER_ID, projectId);
            Assert.assertEquals(EMPTY_REPOSITORY_SIZE, repository.getSize());
            repository.clear();
            session.commit();
        } catch (Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }


    @NotNull
    private Project createOneProject() {
        @NotNull final Project project = new Project();
        project.setId(projectId);
        project.setUserId(TEST_USER_ID);
        project.setName(PROJECT_NAME_PREFIX);
        project.setDescription(PROJECT_DESCRIPTION_PREFIX);
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @NotNull
    private List<Project> createManyProjects() {
        @NotNull final List<Project> projects = new ArrayList<>();
        for (int i = 0; i < REPOSITORY_SIZE; i++) {
            Project project = new Project();
            project.setName(PROJECT_NAME_PREFIX + i);
            project.setDescription(PROJECT_DESCRIPTION_PREFIX + i);
            project.setUserId(TEST_USER_ID);
            projects.add(project);
        }
        return projects;
    }

    @NotNull
    protected SortSpecification getSortSpec(@NotNull Comparator comparator) {
        if (comparator == CreatedComparator.INSTANCE) return created;
        else if (comparator == StatusComparator.INSTANCE) return status;
        else if (comparator == DateBeginComparator.INSTANCE) return dateBegin;
        else return name;
    }


}
